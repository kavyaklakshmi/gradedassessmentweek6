package com.hcl.week6assessment;

import java.util.ArrayList;
import java.util.List;

public class Main{
	public static void main(String[] args) {
		List<Movie> allMovies=new ArrayList<>();
		allMovies.add(new Movie(1, "Amma", "upcoming", "India", 1));
		allMovies.add(new Movie(2, "Avengers", "movies in theatre", "india", 6));
		allMovies.add(new Movie(3, "spiderman", "movies in theatre", "US", 8));
		allMovies.add(new Movie(4, "Amutha", "moviesintheatre", "india", 9));
		allMovies.add(new Movie(5, "", "Avathar", "India", 4));		
		allMovies.add(new Movie(6, "Adipurush", "upcoming", "india", 3));
		
		System.out.println(allMovies);
		//singleton
		MoviesService service=MoviesService.getServiceObject();
		service.setAllMovies(allMovies);
		
		System.out.println(service.getComingSoonMovies());
		
		System.out.println(service.getMoviesInTheatres());
		
		System.out.println(service.getTopRatedIndian());
		
		System.out.println(service.getTopRatedMovies());
		
		System.out.println(service.getMovie(1));
		Movie movie=service.getMovie(2);
		service.addFavMovie(movie);
		System.out.println(service.getFavMovies());
		service.removeFavMovie(movie);
		System.out.println(service.getFavMovies());
	}
}