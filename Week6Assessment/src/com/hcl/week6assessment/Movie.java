package com.hcl.week6assessment;

public class Movie {
	private Integer id;
	private String title;
	private String status;
	private String country;
	private Integer rating;
	
	
	private Movie() {
		super();
	}
	public Movie(Integer id,String title, String status, String country, Integer rating) {
		super();
		this.id=id;
		this.title = title;
		this.status = status;
		this.country = country;
		this.rating = rating;
	}

	
	
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}
	@Override
	public String toString() {
		return "Movie [title=" + title + ", status=" + status + ", country=" + country + ", rating=" + rating + "]\n";
	}
	
	
}


